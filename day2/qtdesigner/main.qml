import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick3D 1.15

View3D {
    Button {
        width: parent.width
        text: "Rotate"
        onClicked: anim.start()
    }

    Model {
        id: cube
        source: "#Cube"
        materials: DefaultMaterial { diffuseColor: "red" }
        NumberAnimation on eulerRotation.x {
            id: anim
            from: 0; to: 360; duration: 1000
        }
    }
    PerspectiveCamera {
        id: camera
        position: "0, 0, 400"
    }
    PointLight {
        position: camera.position
        brightness: 1000
    }
}
