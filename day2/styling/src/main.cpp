/***************************************************************************
**
**  Copyright (C) 2020 by Sandro S. Andrade <sandroandrade@kde.org>
**
**  This file is part of the Qt6 Cookbook.
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include <QApplication>
#include <QFile>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
   QApplication app {argc, argv};
   QApplication::setWindowIcon(QIcon {QStringLiteral(":/icons/qtlogo.svg")});
   QFile file {QStringLiteral(":/style.qss")};
   file.open(QIODevice::ReadOnly);
   app.setStyleSheet(QLatin1String(file.readAll()));
   MainWindow w;

   w.show();

   return QApplication::exec();
}
