import QtQuick 2.15
import Box2D 2.0

Image {
    id: image
    fillMode: Image.PreserveAspectFit

    property alias body: imageBody
    property alias fixture: box
    
    signal beginContact(Fixture other)

    Body {
        id: imageBody
        target: image
        bodyType: Body.Dynamic
        Box {
            id: box
            width: image.width; height: image.height
            restitution: 0.25
            density: 1
            onBeginContact: image.beginContact(other)
        }
    }
}
