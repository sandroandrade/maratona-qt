import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Image {
    id: rootItem
    width: 960; height: 540
    source: "icons/sky.png"
    focus: true
    
    property int points
    
    CheckBox {
        id: rejump
        text: "Enable rejump"
        onCheckedChanged: { upAnim.to = 200; rootItem.focus = true }
    }
    
    Item {
        id: items
        Repeater {
            model: [ "icons/clouds_1.png", "icons/clouds_2.png", "icons/clouds_3.png",
                     "icons/rocks_1.png", "icons/clouds_4.png", "icons/rocks_2.png" ]
            Image { source: modelData }
        }
    }
    
    AnimatedImage {
        id: player
        anchors { bottom: parent.bottom; horizontalCenter: parent.horizontalCenter; bottomMargin: 20 }
        source: !stepTimer.running ? "icons/dying.gif":jumpAnim.running ? "icons/jumping.gif":"icons/running.gif"
        onSourceChanged: playing = true
        SequentialAnimation on anchors.bottomMargin {
            id: jumpAnim
            running: false
            NumberAnimation { id: upAnim; to: 200; duration: 500 }
            NumberAnimation { to: 20; easing.type: Easing.OutBounce; duration: 1000 }
        }
        NumberAnimation on rotation { id: rotationAnim; from: 0; to: 360; running: false }
    }

    AnimatedImage {
        id: enemy
        x: parent.width
        anchors { bottom: parent.bottom; bottomMargin: 20 }
        source: "icons/enemy.gif"
        playing: stepTimer.running
        NumberAnimation on x {
            id: enemyAnim
            to: -enemy.width; duration: 3000
            running: stepTimer.running
            onFinished: {
                enemy.x = parent.width
                enemyAnim.duration = Math.max(250, enemyAnim.duration-250)
                enemyTimer.interval = Math.random()*5000
                enemyTimer.start()
                points++
            }
        }
    }
    
    Keys.onSpacePressed: {
        if (rejump.checked) {
            jumpAnim.stop()
            upAnim.to = player.anchors.bottomMargin + 180
        }
        jumpAnim.start()
    }

    Keys.onPressed: if (event.key === Qt.Key_R) rotationAnim.start()
    
    Timer {
        id: stepTimer
        interval: 1000/24; running: true; repeat: true
        onTriggered: {
            var item
            for (var i = 0; i < items.children.length; ++i) {
                item = items.children[i]
                if (item instanceof Image)
                    item.x = item.x > -parent.width ? item.x-(i+1):0
            }
            if (player.contains(player.mapFromGlobal(enemy.x+33, enemy.y))
                || player.contains(player.mapFromGlobal(enemy.x+enemy.width, enemy.y))
                || player.contains(player.mapFromGlobal(enemy.x+33, enemy.y+enemy.height))
                || player.contains(player.mapFromGlobal(enemy.x+enemy.width, enemy.y+enemy.height))) {
                stepTimer.stop()
                enemyAnim.duration = 3000
            }
        }
    }
    
    Timer {
        id: enemyTimer
        running: stepTimer.running; interval: Math.random()*5000
        onTriggered: enemyAnim.start()
    }
    
    Rectangle {
        id: banner
        width: columnLayout.implicitWidth+80; height: columnLayout.implicitHeight+40
        anchors.centerIn: parent
        color: "red"; radius: 15
        border { width: 3; color: "white" }
        scale: stepTimer.running ? 0:1
        visible: scale !== 0
        ColumnLayout {
            id: columnLayout
            anchors.centerIn: parent
            Label {
                text: "GAME OVER!"; color: "white"; font { bold: true; pixelSize: refLabel.font.pixelSize*2.5 }
                Layout.alignment: Qt.AlignHCenter 
            }
            Label { id: refLabel; text: "Click here to try again!"; color: "white"; font.bold: true; Layout.alignment: Qt.AlignHCenter }
        }
        Behavior on scale { NumberAnimation { duration: 500; easing.type: Easing.OutBounce } }
        MouseArea {
            anchors.fill: parent
            enabled: parent.scale === 1
            onClicked: {
                enemy.x = rootItem.width
                stepTimer.start()
                points = 0
            }
        }
    }
    
    Rectangle {
        width: parent.width/5; height: 35
        anchors { right: parent.right; top: parent.top; rightMargin: 10; topMargin: 10 }
        color: "#472e4a"; radius: 10
        border { width: 2; color: "white" }
        Label { anchors.centerIn: parent; text: "Points: " + points; color: "white"; font.bold: true }
    }
}
