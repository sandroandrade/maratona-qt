import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
//import QtMultimedia 5.15

Window {
    width: 640; height: 640
    visible: true
    title: qsTr("Hello World")

    property int size: 4
    property var emptyItem
    property var shuffled: [...Array(size*size).keys()]//.sort(() => Math.random() - 0.5)
    property int moves: 0

//    MediaPlayer { id: bg; source: "bgloop.mp3"; autoPlay: true; loops: MediaPlayer.Infinite }
//    MediaPlayer { id: whoosh; source: "whoosh.mp3" }
//    MediaPlayer { id: win; source: "win.mp3" }

    Item {
        id: parentItem
        anchors.fill: parent
        Repeater {
            id: repeater
            model: size*size
            Rectangle {
                color: (shuffled[index] === size*size-1) ? "transparent":Qt.hsla(Math.random(1), 1, 0.5, 1)
                x: (index%size)*parent.width/size
                y: Math.trunc(index/size)*parent.height/size
                width: parent.width/size; height: parent.height/size
                Behavior on x { NumberAnimation { duration: 500; easing.type: Easing.OutElastic } }
                Behavior on y { NumberAnimation { duration: 500; easing.type: Easing.OutElastic } }
                Label { anchors.centerIn: parent; text: shuffled[index]+1; color: "white"; font { pixelSize: 48; bold: true } }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (parentItem.childAt(parent.x+1.5*parent.width, parent.y+0.5*parent.height) === emptyItem
                                || parentItem.childAt(parent.x-0.5*parent.width, parent.y+0.5*parent.height) === emptyItem
                                || parentItem.childAt(parent.x+0.5*parent.width, parent.y+1.5*parent.height) === emptyItem
                                || parentItem.childAt(parent.x+0.5*parent.width, parent.y-0.5*parent.height) === emptyItem) {
                            var temp = emptyItem
                            emptyItem.x = parent.x; emptyItem.y = parent.y
                            parent.x = temp.x; parent.y = temp.y
                            //whoosh.stop()
                            //whoosh.play()
                            moves++
                            checkSolutionTimer.start()
                        }
                    }
                }
                Timer {
                    id: checkSolutionTimer
                    interval: 501
                    onTriggered: {
                        var last = 0
                        for (var y = parent.height/2; y < parentItem.height; y += parent.height)
                            for (var x = parent.width/2; x < parentItem.width; x += parent.width)
                                if (parseInt(parentItem.childAt(x, y).children[0].text) !== last+1)
                                    return
                                else
                                    last = parseInt(parentItem.childAt(x, y).children[0].text)
                        bannerRect.scale = 1
                        bannerRect.forceActiveFocus()
                        //bg.stop()
                        //win.play()
                    }
                }
                Component.onCompleted: if (shuffled[index] === size*size-1) emptyItem = this
            }
        }
    }

    Rectangle {
        id: bannerRect
        width: columnLayout.implicitWidth+40; height: columnLayout.implicitHeight+40
        anchors.centerIn: parent
        color: "green"; radius: 15
        border { width: 3; color: "white" }
        scale: 0
        ColumnLayout {
            id: columnLayout
            anchors.centerIn: parent
            Label { text: "Congratulations!"; color: "white"; font { bold: true; pixelSize: refLabel.font.pixelSize*2.5 } Layout.alignment: Qt.AlignHCenter }
            Label { id: refLabel; text: "Click here to try again!"; color: "white"; font.bold: true; Layout.alignment: Qt.AlignHCenter }
        }
        Behavior on scale { NumberAnimation { duration: 500; easing.type: Easing.OutBounce } }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                parent.scale = 0
                shuffled = [...Array(size*size).keys()].sort(() => Math.random() - 0.5)
                for (var i = 0; i < parentItem.children.length; i++)
                    if (parseInt(parentItem.children[i].children[0].text) === size*size) {
                        emptyItem = parentItem.children[i]
                        break
                    }
                repeater.model = size*size
                moves = 0
                //bg.play()
            }
        }
    }

    Rectangle {
        width: parent.width/5; height: 35
        anchors { right: parent.right; top: parent.top; rightMargin: 10; topMargin: 10 }
        color: "blue"; radius: 10
        border { width: 3; color: "white" }
        Label { anchors.centerIn: parent; text: moves + " moves"; color: "white"; font.bold: true }
    }
}
